<?php
require_once 'init.php';
if(empty(($_SESSION['user']['login']))){
    header('Location: loginForm.php');
    exit();
}
?>
<?php
if (!empty($_GET['action']) && !empty($_GET['id'])){
    include 'action.php';
}
?>

<?php get_header(); ?>

<?php if($_SESSION['user']['role']==='admin'):?>
<div class="container">
    <div class="row justify-content-center" style="margin-top: 30vh;">
        <?php
        if(!empty($_SESSION['error']) || !empty($_POST['error'])){
            printf('<small><p style="color: red"> %s </p></small>', $_SESSION['error']);
            unset($_SESSION['error']);
        }//margin: 200px auto; width:300px;
        ?>
        <table class="table table-hover">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Login</th>
                    <th scope="col">Email</th>
                    <th scope="col">Wiek</th>
                    <th scope="col">Telefon</th>
                    <th scope="col">Miejscowość</th>
                    <th scope="col">Rola</th>
                    <th scope="col" class='text-center'>Akcja</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    include_once 'connect_db.php';
                    $query = "SELECT login, email, wiek, telefon, miejscowosc, role FROM `users`";
                    $res = $mysqli->query($query);
                    if ($res->num_rows <= 0){
                        echo '<tr><td>Nic tu nie ma</td></tr>';
                    }
                    else{
                        $i = 1;
                        foreach($res->fetch_all() as $value){

                            echo '<tr>';
                                echo "<td>$i</td>";

                                if(!empty($_GET['action']) && !empty($_GET['id']) && strcmp($_GET['action'],'edit')===0 && $i === (int)$_GET['id']) {
                                    edit($value, $i);
                                    continue;
                                }

                                foreach ($value as $item){
                                    echo '<td>';
                                    echo $item;
                                    echo '</td>';
                                }
                                echo "<td class='text-center'>";
                                echo "<a href='?action=edit&id=$i'><i class='fas fa-pencil-alt mx-1'></i></a>";
                                echo "<a href='deleteUser.php?user=$value[0]'><i class='fas fa-trash-alt mx-1'></i></a>";
                                echo "</td>";
                            echo '</tr>';
                            $i++;
                        }
                    }
                ?>

            </tbody>
        </table>
    </div>
</div>
<?php endif;?>
<?php if($_SESSION['user']['role']==='user'):?>
    <div class="container">
        <div class="row justify-content-center" style="margin-top: 30vh;">
            <?php
            if(!empty($_SESSION['error']) || !empty($_POST['error'])){
                printf('<small><p style="color: red"> %s </p></small>', $_SESSION['error']);
                unset($_SESSION['error']);
            }//margin: 200px auto; width:300px;
            ?>
            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Login</th>
                    <th scope="col">Email</th>
                    <th scope="col">Wiek</th>
                    <th scope="col">Telefon</th>
                    <th scope="col">Miejscowość</th>
                    <?php if($_SESSION['user']['role'] === 'admin'):?>
                    <th scope="col">Rola</th>
                    <?php endif;?>
                    <th scope="col" class='text-center'>Akcja</th>
                </tr>
                </thead>
                <tbody>
                <?php
                include_once 'connect_db.php';
                if($_SESSION['user']['role'] === 'admin'){
                    $query = "SELECT login, email, wiek, telefon, miejscowosc, role FROM `users` WHERE login = '{$_SESSION['user']['login']}'";
                }
                else{
                    $query = "SELECT login, email, wiek, telefon, miejscowosc FROM `users` WHERE login = '{$_SESSION['user']['login']}'";
                }
                $res = $mysqli->query($query);
                if ($res->num_rows <= 0){
                    echo '<tr><td>Nic tu nie ma</td></tr>';
                }
                else{
                    $i = 1;
                    foreach($res->fetch_all() as $value){

                        echo '<tr>';
                        echo "<td>$i</td>";

                        if(!empty($_GET['action']) && !empty($_GET['id']) && strcmp($_GET['action'],'edit')===0 && $i === (int)$_GET['id']) {
                            edit($value, $i);
                            continue;
                        }

                        foreach ($value as $item){
                            echo '<td>';
                            echo $item;
                            echo '</td>';
                        }
                        echo "<td class='text-center'>";
                        echo "<a href='?action=edit&id=$i'><i class='fas fa-pencil-alt mx-1'></i></a>";
                        echo "<a href='deleteUser.php?user=$value[0]'><i class='fas fa-trash-alt mx-1'></i></a>";
                        echo "</td>";
                        echo '</tr>';
                        $i++;
                    }
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
<?php endif;?>

<?php get_footer();?>
