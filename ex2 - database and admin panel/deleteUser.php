<?php
require_once 'init.php';
require_once 'connect_db.php';

if(empty($_SESSION['user']['login'])){
    header('Location: loginForm.php');
    exit();
}

$user = $_GET['user'];

if($_SESSION['user']['role']==='user'){
    if($_SESSION['user']['login']!==$user){
        $_SESSION['error'] = 'Brak dostępu';
        header('Location: account.php');
        exit();
    }
    $query = "DELETE FROM `users` WHERE login = '$user'";
    $res = $mysqli->query($query);
    if($res){
        header('Location: logout.php');
        exit();
    }
    else{
        $_SESSION['error'] = 'Konto nie usunięte. Błąd.';
        header('Location: account.php');
        exit();
    }

}

if($_SESSION['user']['role']==='admin'){
    $query = "DELETE FROM `users` WHERE login = '$user'";
    $res = $mysqli->query($query);
    if($res){
        if($user === $_SESSION['user']['login']){
            header('Location: logout.php');
            exit();
        }
        header('Location: account.php');
        exit();
    }
    else{
        $_SESSION['error'] = 'Konto nie usunięte. Błąd.';
        header('Location: account.php');
        exit();
    }
}