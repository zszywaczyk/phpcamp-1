<?php
require_once 'init.php';
if( !empty($_SESSION['user'])){
    header('Location: account.php');
    exit();
}
?>
<?php get_header();?>


        <div class="container">
            <div class="row justify-content-center" style="margin-top: 30vh;">
                <div class="card">
                    <div class="card-header text-center" style="width: 300px">
                        <h4>Zaloguj się</h4>
                    </div>
                    <div class="card-body" style="width: 300px">
                        <form action="login.php" method="POST" style="" class="bg-light rounded-lg">
                            <?php
                            if(!empty($_SESSION['error']) || !empty($_POST['error'])){
                                printf('<small><p style="color: red"> %s </p></small>', $_SESSION['error']);
                                unset($_SESSION['error']);
                            }//margin: 200px auto; width:300px;
                            ?>


                                <div class="form-group mb-1">
                                    <label for="userLog" class="mb-0"><small>Login:</small></label>
                                    <input type="text" name="login"
                                           value="<?php if(!empty($_SESSION['used_login'])){ echo $_SESSION['used_login']; unset($_SESSION['used_login']); }?>"
                                           id="userLog" class="form-control" placeholder="Login">
                                </div>
                                <div class="form-group">
                                    <label for="userPass" class="mb-0"><small>Hasło:</small></label>
                                    <input type="password" name="password" id="userPass" class="form-control" placeholder="Hasło">
                                </div>
                                <input type="submit" class="btn btn-primary w-100 mt-3" value="Zaloguj się">

                        </form>
                        <button type="button" class="btn btn-primary w-100 mt-3" data-toggle="modal" data-target="#exampleModal">Stwórz konto</button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form method="post" action="createUser.php">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Stwórz swoje konto</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="form-group mb-1">
                                            <label for="loginRegister" class="mb-0"><small><span style="color: red;">*</span>Login:</small></label>
                                            <input type="text" name="login" class="form-control" id="loginRegister" placeholder="login" required>
                                        </div>
                                        <div class="form-group mb-1">
                                            <label for="passRegister" class="mb-0"><small><span style="color: red;">*</span>Hasło:</small></label>
                                            <input type="password" name="pass" class="form-control" id="passRegister" placeholder="hasło" required>
                                        </div>
                                        <div class="form-group mb-1">
                                            <label for="emailRegister" class="mb-0"><small><span style="color: red;">*</span>Email:</small></label>
                                            <input type="email" name="email" class="form-control" id="emailRegister" placeholder="email" required>
                                        </div>
                                        <div class="form-group mb-1">
                                            <label for="ageRegister" class="mb-0"><small>Wiek:</small></label>
                                            <input type="number" name="age" class="form-control" id="ageRegister" placeholder="wiek">
                                        </div>
                                        <div class="form-group mb-1">
                                            <label for="phoneRegister" class="mb-0"><small>Telefon:</small></label>
                                            <input type="tel" name="phone" class="form-control" id="phoneRegister" placeholder="telefon">
                                        </div>
                                        <div class="form-group mb-1">
                                            <label for="cityRegister" class="mb-0"><small>Miejscowość:</small></label>
                                            <input type="text" name="city" class="form-control" id="cityRegister" placeholder="miasto">
                                        </div>

                                    </div>
                                    <div class="modal-footer justify-content-center">
                                        <button type="submit" class="btn btn-primary" formmethod="post">Stwórz</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


<?php get_footer(); ?>