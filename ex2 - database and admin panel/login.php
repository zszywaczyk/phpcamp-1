<?php
require_once 'init.php';
if(!empty(($_SESSION['user']['login']))){
    header('Location: account.php');
    exit();
}
if(empty($_POST['login']) && empty($_POST['password'])){
    $_SESSION['error'] = 'Nie podałeś loginu ani hasła';
    header('Location: loginForm.php');
    exit();
}
if(empty($_POST['login']) && !empty($_POST['password'])){
    $_SESSION['error'] = 'Nie podałeś loginu';
    header('Location: loginForm.php');
    exit();
}
if(!empty($_POST['login']) && empty($_POST['password'])){
    $_SESSION['error'] = 'Nie podałeś hasła';
    $_SESSION['used_login'] = $_POST['login'];
    header('Location: loginForm.php');
    exit();
}
//valid
include 'connect_db.php';
$login = $_POST['login'];
$password = $_POST['password'];

$query = "SELECT login, email, wiek, telefon, miejscowosc, role FROM `users` WHERE login = '$login' AND pass = '$password'";
$res = $mysqli->query($query);

if($res->num_rows === 1){
    $_SESSION['user'] = $res->fetch_assoc();
    $res->close();
    $mysqli->close();
    header('Location: account.php');
    exit();
}

$_SESSION['used_login'] = $_POST['login'];
$_SESSION['error'] = 'Nieprawidłowy login lub hasło';
$res->close();
$mysqli->close();
header('Location: loginForm.php');
exit();
