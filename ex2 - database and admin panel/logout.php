<?php
require_once 'init.php';
if(empty($_SESSION['user'])){
    header('Location: loginForm.php');
    exit();
}
unset($_SESSION['user']);
header('Location: loginForm.php');
exit();