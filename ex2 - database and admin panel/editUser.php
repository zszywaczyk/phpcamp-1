<?php
require_once 'init.php';
require_once 'connect_db.php';

if(empty($_SESSION['user']['login'])){
    header('Location: loginForm.php');
    exit();
}
if(!isset($_POST['a'.'0']) || !isset($_POST['a'.'1']) || !isset($_POST['a'.'2']) || !isset($_POST['a'.'3']) || !isset($_POST['a'.'4'])){
    $_SESSION['error'] = 'Błąd';
    header('Location: account.php');
    exit();
}

$user = $_GET['user'];
$var = $_POST;

if($_SESSION['user']['role']==='user'){
    if($_SESSION['user']['login']!==$user){
        $_SESSION['error'] = 'Brak dostępu';
        header('Location: account.php');
        exit();
    }
    $query = "UPDATE users SET `login` = '{$var['a'.'0']}', `email` = '{$var['a'.'1']}', `wiek` = '{$var['a'.'2']}', `telefon` = '{$var['a'.'3']}', `miejscowosc` = '{$var['a'.'4']}' WHERE `login` = '$user'";
    $res = $mysqli->query($query);
    if($res){
        $query = "SELECT login, email, wiek, telefon, miejscowosc, role FROM `users` WHERE login = '{$var['a'.'0']}'";
        $res = $mysqli->query($query);
        if($res->num_rows === 1){
            $_SESSION['user'] = $res->fetch_assoc();
            $res->close();
            $mysqli->close();
            header('Location: account.php');
            exit();
        }
        exit();
    }
    else{
        $_SESSION['error'] = 'Operacja nie powiodła sie. Błąd.';
        header('Location: account.php');
        exit();
    }

}
if(!isset($_POST['a'.'5'])){
    $_SESSION['error'] = 'Brakuje 6 parmetru';
    header('Location: account.php');
    exit();
}
if($_SESSION['user']['role']==='admin'){
    $query = "UPDATE users SET `login` = '{$var['a'.'0']}', `email` = '{$var['a'.'1']}', `wiek` = '{$var['a'.'2']}', `telefon` = '{$var['a'.'3']}', `miejscowosc` = '{$var['a'.'4']}', `role` = '{$var['a'.'5']}' WHERE `login` = '$user'";
    $res = $mysqli->query($query);
    if($res){
        if($user === $_SESSION['user']['login']){
            $query = "SELECT login, email, wiek, telefon, miejscowosc, role FROM `users` WHERE login = '{$var['a'.'0']}'";
            $res = $mysqli->query($query);
            $_SESSION['user'] = $res->fetch_assoc();
            $res->close();
            $mysqli->close();
            header('Location: account.php');
            exit();
        }
        header('Location: account.php');
        exit();
    }
    else{
        $_SESSION['error'] = 'Konto nie usunięte. Błąd.';
        //var_dump($mysqli->error_list);
        header('Location: account.php');
        exit();
    }
}