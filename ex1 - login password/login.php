<?php
require_once 'init.php';
if(!empty(($_SESSION['user']['login']))){
    header('Location: account.php');
    exit();
}
if(empty($_POST['login']) && empty($_POST['password'])){
    $_SESSION['error'] = 'Nie podałeś loginu ani hasła';
    header('Location: loginForm.php');
    exit();
}
if(empty($_POST['login']) && !empty($_POST['password'])){
    $_SESSION['error'] = 'Nie podałeś loginu';
    header('Location: loginForm.php');
    exit();
}
if(!empty($_POST['login']) && empty($_POST['password'])){
    $_SESSION['error'] = 'Nie podałeś hasła';
    $_SESSION['used_login'] = $_POST['login'];
    header('Location: loginForm.php');
    exit();
}
//valid
$login = $_POST['login'];
$password = $_POST['password'];

$file = file_get_contents('userdb');
$rows = explode("\n", $file);
foreach ($rows as $key=> $row){
    $rows[$key] = explode(' ',$row);
    if(strcmp($rows[$key][0], $login) === 0){
        if(strcmp($rows[$key][1], $password) === 0){
            $_SESSION['user']['login'] = $rows[$key][0];
            header('Location: account.php');
            exit();
        }
    }

}
$_SESSION['used_login'] = $_POST['login'];
$_SESSION['error'] = 'Nieprawidłowy login lub hasło';
header('Location: loginForm.php');
exit();
