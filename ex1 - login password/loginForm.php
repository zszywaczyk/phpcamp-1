<?php
require_once 'init.php';
if( !empty($_SESSION['user'])){
    header('Location: account.php');
    exit();
}
?>
<html>
<head></head>
<body>

    <form action="login.php" method="POST" style="margin: 200px auto; width:300px;">
        <?php
        if(!empty($_SESSION['error']) || !empty($_POST['error'])){
            printf('<p style="color: red"> %s </p>', $_SESSION['error']);
            unset($_SESSION['error']);
        }
        ?>
        <table>
            <tr>
                <td><label for="userLog" style="margin-bottom: 5px;" >Login:</label></td>
                <td><input type="text" name="login" id="userLog" style="margin: 5px 0;"
                           value="<?php if(!empty($_SESSION['used_login'])){ echo $_SESSION['used_login']; unset($_SESSION['used_login']); }?>"
                    ><br></td>
            </tr>
            <tr>
                <td><label for="userPass">Password: </label></td>
                <td><input type="password" name="password" id="userPass" style="margin: 5px 0;"></td>
            </tr>
        </table>
        <div style="text-align: center; margin-top: 10px">
        <input type="submit" value="Zaloguj" style="margin: 0 auto; padding: 0 50px;">
        </div>
    </form>
</body>
</html>